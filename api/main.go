package main

import (
	"developmental-assignment/config"

	"developmental-assignment/db"

	_ "github.com/lib/pq"
)

func main() {
	cfg, err := config.NewConfig("config.yaml")

	if err != nil {
		panic(err)
	}

	if err := db.MigrateDB(cfg); err != nil {
		panic(err)
	}

	server, err := buildServer(cfg)

	if err != nil {
		panic(err)
	}

	server.Start()
}
