package config

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// NewConfig parses the config file from the given path
func NewConfig(path string) (*Config, error) {
	cfg := &Config{}
	contents, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(contents, cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

// Config holds app config
type Config struct {
	DB struct {
		Host     string
		Port     int
		Username string
		Password string
		Name     string
		Pool     struct {
			MinOpen int `yaml:"minOpen"`
			MaxOpen int `yaml:"maxOpen"`
		}
	}
	CORS struct {
		Debug          bool
		AllowedOrigins []string `yaml:"allowedOrigins"`
	} `yaml:"cors"`
	Gorm struct {
		Migrate bool
		LogMode bool `yaml:"logMode"`
	}
	HTTP struct {
		Port string
	}
	GraphQL struct {
		Playground bool
	} `yaml:"graphql"`
	Facebook struct {
		VerifyToken string `yaml:"verifyToken"`
	} `yaml:"facebook"`
	Slack struct {
		BaseURL string `yaml:"baseURL"`
		PostMessage string `yaml:"postMessage"`
		Token string `yaml:"token"`
		Channel string `yaml:"channel"`
	} `yaml:"slack"`
	Chatfuel struct {
		BroadcastURL string `yaml:"broadcastURL"`
		Token string `yaml:"token"`
		MessageTag string `yaml:"messageTag"`
		PromoBlock string `yaml:"promoBlock"`
		BotID string `yaml:"botID"`
	} `yaml:"chatfuel"`
}
