module developmental-assignment

go 1.12

require (
	github.com/99designs/gqlgen v0.8.3
	github.com/denisenkom/go-mssqldb v0.0.0-20190514213226-23b29e59681b // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/go-siris/siris v7.4.0+incompatible // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/google/wire v0.5.0
	github.com/gorilla/mux v1.6.2
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v1.0.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.2.8 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/onsi/ginkgo v1.15.1
	github.com/onsi/gomega v1.11.0
	github.com/oxequa/interact v0.0.0-20171114182912-f8fb5795b5d7 // indirect
	github.com/oxequa/realize v2.0.2+incompatible // indirect
	github.com/rs/cors v1.6.0
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/valyala/fasttemplate v1.0.1 // indirect
	github.com/vektah/gqlparser v1.1.2
	go.uber.org/zap v1.16.0
	gopkg.in/go-playground/validator.v9 v9.28.0
	gopkg.in/urfave/cli.v2 v2.0.0-20180128182452-d3ae77c26ac8 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
