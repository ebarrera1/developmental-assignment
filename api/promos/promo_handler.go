package promos

import (
	"developmental-assignment/config"
	"developmental-assignment/customers"
	"developmental-assignment/handlers/response"
	"developmental-assignment/model"
	"net/http"
	"github.com/gorilla/mux"
	"fmt"
	"go.uber.org/zap"
	"strings"

)
var logger, _ = zap.NewProduction()

func NewPromoHandler(cfg *config.Config,
promoService PromoService,
customerService customers.CustomerService	) *PromoHandler {
	return &PromoHandler{
		config:cfg,
		promoService:promoService,
		customerService: customerService,

	}
}

type PromoHandler struct {
	config *config.Config
	promoService PromoService
	customerService customers.CustomerService

}


func (p *PromoHandler) CreatePromo(w http.ResponseWriter, r *http.Request) {
	message := r.FormValue("promo")
	promo:= &Promo{}
	promo.SetMessage(message)
	err := p.promoService.Create(promo)
	if err != nil {
			response.Error(err, w)
			return
		}
}

func (p *PromoHandler) TriggerPromo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	params := vars["promoID"]
	fmt.Println(params)
	promo,err:= p.promoService.GetByPromoID(params,nil)
	if err != nil {
		response.Error(err, w)
		return
	}
	customers, err := p.customerService.ListAll(model.NewListOpts().SetPreload())
	for _, customer := range customers.Items() {

		url :=p.config.Chatfuel.BroadcastURL + 
		p.config.Chatfuel.BotID+"/users/"+customer.GetFBID()+"/send?chatfuel_token="+p.config.Chatfuel.Token +
		"&chatfuel_message_tag="+p.config.Chatfuel.MessageTag+"&chatfuel_block_name="+p.config.Chatfuel.PromoBlock +
		"&promo="+ strings.ReplaceAll(promo.GetMessage()," ","%20")
		fmt.Println(url)

		req, err := http.NewRequest(http.MethodPost,url,nil)
		fmt.Println(promo)
		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			logger.Error(err.Error())
		}
			
		defer resp.Body.Close()
}
	w.Write([]byte("done"))
	w.WriteHeader(200)

}

