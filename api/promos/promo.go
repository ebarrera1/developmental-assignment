package promos

import (
	"developmental-assignment/model"
	"developmental-assignment/customers"
)

type Promo struct{
	model.BaseModel
	Message string `gorm:"type:varchar(255) null"`
	Customer [] *customers.Customer `gorm:"many2many:recipient;"`
}

func (p *Promo) GetMessage() string {
	return p.Message
}

func (p *Promo) SetMessage(arg string) *Promo {
	p.Message = arg
	return p
}

