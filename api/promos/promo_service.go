package promos

import "developmental-assignment/model"


func NewService(store PromoStore) PromoService {
	return &promoService{store: store}
}

type PromoService interface {
	Create(promo *Promo) error
	GetByPromoID(id string, opts model.GetOpts) (*Promo, error)
}

type promoService struct {
	store PromoStore
}

func (p *promoService) Create(promo *Promo) error {
	return p.store.Create(promo)
}

func (p *promoService) GetByPromoID(id string, opts model.GetOpts) (*Promo, error) {
	return p.store.GetByPromoID(id, opts)
}