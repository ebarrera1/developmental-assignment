package promos

import (
	"developmental-assignment/model"

	"github.com/jinzhu/gorm"
)

func NewStore(db *gorm.DB) PromoStore {
	return &promoStore{model.BaseStore{
		DB: db,
	}}
}

type PromoStore interface {
	Create(promo *Promo) error
	GetByPromoID(id string, opts model.GetOpts) (*Promo, error)
}

type promoStore struct {
	model.BaseStore
}

func (p *promoStore) Create(promo *Promo) error {
	err := p.BaseStore.Create(promo)
	return err
}

func (p *promoStore) GetByPromoID(id string, opts model.GetOpts) (*Promo, error) {
	db := p.DB.Where("id = ?", id)
	promo, err := p.Find(db, &Promo{}, opts)

	if err != nil {
		return nil, err
	}

	return promo.(*Promo), nil
}