package main

import (
	"developmental-assignment/chatfuel"
	"developmental-assignment/config"
	"developmental-assignment/customers"
	"developmental-assignment/db"
	"developmental-assignment/facebook"
	"developmental-assignment/promos"
	"developmental-assignment/slack"

	"github.com/google/wire"
)

var DependencySet = wire.NewSet(
	db.NewConn,
	chatfuel.NewChatfuelHandler,
	customers.NewStore,
	customers.NewService,
	facebook.NewFacebookHandler,
	promos.NewStore,
	promos.NewService,
	promos.NewPromoHandler,
	slack.NewSlackHandler,
	NewServer)

func buildServer(cfg *config.Config) (Server, error) {
	wire.Build(DependencySet)
	return nil, nil
}
