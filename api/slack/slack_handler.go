package slack


import (
	"developmental-assignment/config"
	"net/http"
	"net/url"
	"strings"
	"go.uber.org/zap"

)

var logger, _ = zap.NewProduction()

func NewSlackHandler(cfg *config.Config,
) *SlackHandler {
	return &SlackHandler{
		config: cfg,
	}
}

type SlackHandler struct {
	config *config.Config
}

func (s *SlackHandler) SendNotification(w http.ResponseWriter, r *http.Request) {
	
	fullName := r.FormValue("fbFname") + " " + r.FormValue("fbLname")
	message:= fullName + " wants to talk to the manager"
	data:=url.Values{}
	data.Set("token",s.config.Slack.Token)
	data.Set("channel",s.config.Slack.Channel)
	data.Set("text",message)

	req, err := http.NewRequest(http.MethodPost, s.config.Slack.BaseURL + "/"+s.config.Slack.PostMessage, strings.NewReader(data.Encode()))
	if err != nil {
		 logger.Error(err.Error())
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Error(err.Error())
	}
	defer resp.Body.Close()
	w.Write([]byte("done"))
	w.WriteHeader(200)

}