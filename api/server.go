package main

import (
	"developmental-assignment/chatfuel"
	"developmental-assignment/config"
	"developmental-assignment/facebook"
	"developmental-assignment/promos"
	"developmental-assignment/slack"
	"developmental-assignment/handlers"
	"net/http"

	"github.com/gorilla/mux"
)

func NewServer(cfg *config.Config,
	chatfuelHandler *chatfuel.ChatfuelHandler,
	facebookHandler *facebook.FacebookHandler,
	promoHandler *promos.PromoHandler,
	slackHandler *slack.SlackHandler,

) Server {
	return &server{
		config:          cfg,
		chatfuelHandler: chatfuelHandler,
		facebookHandler: facebookHandler,
		promoHandler:promoHandler,
		slackHandler: slackHandler,

	}
}

type Server interface {
	Start()
}

type server struct {
	config          *config.Config
	chatfuelHandler *chatfuel.ChatfuelHandler
	facebookHandler *facebook.FacebookHandler
	promoHandler *promos.PromoHandler
	slackHandler *slack.SlackHandler

}

func (s *server) Start() {
	r := mux.NewRouter()
	http.Handle("/", r)
	r.HandleFunc("/chatfuel/create", s.chatfuelHandler.CreateUser).Methods("POST")
	r.HandleFunc("/health", handlers.Health).Methods("GET")
	// facebook api webhooks
	r.HandleFunc("/webhooks", s.facebookHandler.PostWebhook).Methods("POST")
	r.HandleFunc("/webhooks", s.facebookHandler.GetWebhook).Methods("GET")
	
	// promo
	r.HandleFunc("/promo/create", s.promoHandler.CreatePromo).Methods("POST")
	r.HandleFunc("/promo/trigger/{promoID}", s.promoHandler.TriggerPromo).Methods("GET")

	// slack
	r.HandleFunc("/sendNotification", s.slackHandler.SendNotification).Methods("POST")

	http.ListenAndServe(s.config.HTTP.Port, nil)
}
