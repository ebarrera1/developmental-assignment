package db

import (
	"developmental-assignment/recipients"
	"fmt"

	"developmental-assignment/config"
	"developmental-assignment/customers"
	"developmental-assignment/promos"

	"github.com/jinzhu/gorm"
)

// NewConn Creates a database connection
func NewConn(config *config.Config) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.DB.Host,
		config.DB.Port,
		config.DB.Username,
		config.DB.Password,
		config.DB.Name,
	))

	if err != nil {
		return nil, err
	}

	db.LogMode(config.Gorm.LogMode)
	db.SingularTable(true)

	return db, nil
}

// MigrateDB performs migrations
func MigrateDB(config *config.Config) error {
	if !config.Gorm.Migrate {
		return nil
	}

	db, err := NewConn(config)

	if err != nil {
		return err
	}

	models := []interface{}{
		&customers.Customer{},
		&promos.Promo{},
		&recipients.Recipient{},
	}
	
	if err := db.AutoMigrate(models...).Error; err != nil {
		panic(err)
	}

	return nil
}
