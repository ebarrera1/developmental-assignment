package recipients



type Recipient struct {

	Status string            `gorm:"type:varchar(100)"`
}

func (r *Recipient) GetStatus() string {
	return r.Status
}

func (r *Recipient) SetStatus(arg string) *Recipient {
	r.Status = arg
	return r
}



