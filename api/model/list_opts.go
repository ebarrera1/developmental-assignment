package model

import "strings"

func NewListOpts() ListOpts {
	opts := &BaseListOpts{}
	opts.Init()
	return opts
}

type ListOpts interface {
	By() interface{}
	SetBy(arg interface{}) ListOpts
	Offset() *int
	SetOffset(*int) ListOpts
	SetOffsetVal(int) ListOpts
	Max() *int
	SetMax(*int) ListOpts
	SetMaxVal(int) ListOpts
	Sort() []*Sort
	SortString() string
	SetSort([]*Sort) ListOpts
	Preload() []string
	SetPreload(...string) ListOpts
}

// ListOpts represents listing options
type BaseListOpts struct {
	offset  *int
	max     *int
	sort    []*Sort
	by      interface{}
	preload []string
}

func (b *BaseListOpts) Preload() []string {
	return b.preload
}

func (b *BaseListOpts) SetPreload(arg ...string) ListOpts {
	for _, p := range arg {
		b.preload = append(b.preload, p)
	}

	return b
}

func (b *BaseListOpts) Init() {
	b.offset = nil
	b.max = nil
}

func (b *BaseListOpts) By() interface{} {
	return b.by
}

func (l *BaseListOpts) SetBy(by interface{}) ListOpts {
	l.by = by
	return l
}

func (b *BaseListOpts) Offset() *int {
	return b.offset
}

func (b *BaseListOpts) SetOffset(arg *int) ListOpts {
	b.offset = arg
	return b
}

func (b *BaseListOpts) SetOffsetVal(arg int) ListOpts {
	b.offset = &arg
	return b
}

func (b *BaseListOpts) Max() *int {
	return b.max
}

func (b *BaseListOpts) SetMax(arg *int) ListOpts {
	b.max = arg
	return b
}

func (b *BaseListOpts) SetMaxVal(arg int) ListOpts {
	b.max = &arg
	return b
}

func (b *BaseListOpts) Sort() []*Sort {
	return b.sort
}

func (b *BaseListOpts) SortString() string {
	orderBy := []string{}

	for _, sort := range b.Sort() {
		order := "asc"

		if sort.Order() == SortOrderDesc {
			order = "desc"
		}

		orderBy = append(orderBy, sort.Name()+" "+order)
	}

	return strings.Join(orderBy, ", ")
}

func (b *BaseListOpts) SetSort(arg []*Sort) ListOpts {
	b.sort = arg
	return b
}
