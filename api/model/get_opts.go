package model

func NewGetOpts() GetOpts {
	return &getOpts{}
}

type GetOpts interface {
	Preload() []string
	SetPreload(arg ...string) GetOpts
}

type getOpts struct {
	preload []string
}

func (g *getOpts) Preload() []string {
	return g.preload
}

func (g *getOpts) SetPreload(arg ...string) GetOpts {
	for _, p := range arg {
		g.preload = append(g.preload, p)
	}

	return g
}
