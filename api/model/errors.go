package model

import (
	"net/http"
)

var (
	ErrNotFound         = NewError("NOT_FOUND", http.StatusNotFound)
	ErrInvalidArg       = NewError("INVALID_ARG", http.StatusBadRequest)
	ErrInvalidForm      = NewError("INVALID_FORM", http.StatusBadRequest)
	ErrConcurrentUpdate = NewError("CONCURRENT_UPDATE", http.StatusBadRequest)
	ErrForbidden        = NewError("FORBIDDEN", http.StatusForbidden)
)
