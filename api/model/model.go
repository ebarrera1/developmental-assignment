package model

import (
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Model interface {
	GetID() string
	GetVersion() int
	GetCreated() time.Time
	GetUpdated() time.Time
	Validate() error
}

// BaseModel base Model implementation
type BaseModel struct {
	ID      string    `gorm:"primary_key;type:varchar(36) not null"`
	Version int       `gorm:"type:int not null"`
	Created time.Time `gorm:"type:timestamp not null"`
	Updated time.Time `gorm:"type:timestamp not null"`
}

// BeforeCreate runs before model is inserted
func (m *BaseModel) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("ID", uuid.New().String())
	scope.SetColumn("Version", 0)
	scope.SetColumn("Created", time.Now().UTC())
	scope.SetColumn("Updated", time.Now().UTC())
	return nil
}

func (m *BaseModel) BeforeDelete(scope *gorm.Scope) error {
	return nil
}

func (m *BaseModel) BeforeUpdate(scope *gorm.Scope) error {
	scope.SetColumn("Version", m.Version+1)
	scope.SetColumn("Updated", time.Now().UTC())
	return nil
}

func (m *BaseModel) GetID() string {
	return m.ID
}

func (m *BaseModel) GetVersion() int {
	return m.Version
}

func (m *BaseModel) GetCreated() time.Time {
	return m.Created
}

func (m *BaseModel) GetUpdated() time.Time {
	return m.Updated
}

func (m *BaseModel) Validate() error {
	return nil
}
