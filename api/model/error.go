package model

import (
	"fmt"
)

type Error struct {
	Code   string
	Status int
}

func NewError(code string, status int) *Error {
	return &Error{
		Code:   code,
		Status: status,
	}
}

func (a *Error) Error() string {
	return fmt.Sprintf("Error %d: %s", a.Status, a.Code)
}
