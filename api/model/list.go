package model

// List contains list of models
type List interface {
	Total() int
	SetTotal(int)
	ItemsPtr() interface{}
	Model() Model
}

// BaseList base List implementation
type BaseList struct {
	total int
}

func (b *BaseList) Total() int {
	return b.total
}

func (b *BaseList) SetTotal(arg int) {
	b.total = arg
}

func (b *BaseList) Init() {
	b.SetTotal(0)
}
