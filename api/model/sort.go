package model

func NewSort(name string, order int) *Sort {
	return &Sort{name: name, order: order}
}

type Sort struct {
	name  string
	order int
}

func (s *Sort) Name() string {
	return s.name
}

func (s *Sort) SetName(arg string) *Sort {
	s.name = arg
	return s
}

func (s *Sort) Order() int {
	return s.order
}

const (
	SortOrderAsc  = iota
	SortOrderDesc = iota
)
