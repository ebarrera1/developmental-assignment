package facebook

import (
	"developmental-assignment/config"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func NewFacebookHandler(cfg *config.Config,
) *FacebookHandler {
	return &FacebookHandler{
		config: cfg,
	}
}

type FacebookHandler struct {
	config *config.Config
}

func (f *FacebookHandler) PostWebhook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Called POST")
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	bodyString := string(bodyBytes)
	fmt.Println(bodyString)

}

func (f *FacebookHandler) GetWebhook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Called GET")
	fmt.Println(r.URL.Query().Get("hub.mode"))
	fmt.Println(r.URL.Query().Get("hub.verify_token"))
	fmt.Println(r.URL.Query().Get("hub.challenge"))
	var mode = r.URL.Query().Get("hub.mode")
	var token = r.URL.Query().Get("hub.verify_token")
	var challenge = r.URL.Query().Get("hub.challenge")

	// Checks the mode and token sent is correct
	if mode == "subscribe" && token == f.config.Facebook.VerifyToken {

		// Responds with the challenge token from the request
		fmt.Println("WEBHOOK VERIFIED")
		w.Write([]byte(challenge))
		w.WriteHeader(200)

	} else {
		w.WriteHeader(403)
	}

}
