package chatfuel

import (
	"developmental-assignment/config"
	"developmental-assignment/customers"
	"developmental-assignment/handlers/response"
	"net/http"
)

func NewChatfuelHandler(cfg *config.Config,
	customerService customers.CustomerService,
) *ChatfuelHandler {
	return &ChatfuelHandler{
		config:          cfg,
		customerService: customerService,
	}
}

type ChatfuelHandler struct {
	config          *config.Config
	customerService customers.CustomerService
}

func (c *ChatfuelHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	fullName := r.FormValue("fbFname") + " " + r.FormValue("fbLname")
	customer := &customers.Customer{}
	customer.SetFBName(fullName).
		SetFBID(r.FormValue("fbID")).
		SetBirthDate(r.FormValue("birthDate"))
	err := c.customerService.Create(customer)
	if err != nil {
		response.Error(err, w)
		return
	}
}
