package customers

import (
	"developmental-assignment/model"

	"github.com/jinzhu/gorm"
)

func NewStore(db *gorm.DB) CustomerStore {
	return &customerStore{model.BaseStore{
		DB: db,
	}}
}

type CustomerStore interface {
	Create(customer *Customer) error
	GetByMessengerID(id string, opts model.GetOpts) (*Customer, error)
	ListAll(opts model.ListOpts) (*CustomerList, error)

}

type customerStore struct {
	model.BaseStore
}

func (c *customerStore) Create(customer *Customer) error {
	err := c.BaseStore.Create(customer)
	return err
}

func (c *customerStore) GetByMessengerID(id string, opts model.GetOpts) (*Customer, error) {
	db := c.DB.Where("fb_id = ?", id)
	customer, err := c.Find(db, &Customer{}, opts)

	if err != nil {
		return nil, err
	}

	return customer.(*Customer), nil
}

func (c *customerStore) ListAll(opts model.ListOpts) (*CustomerList, error) {
	db := c.DB.Where("fb_id != ''")
	list := NewList()

	err := c.FindAll(db ,list,opts)

	if err != nil {
		return nil, err
	}

	return list,err
}

