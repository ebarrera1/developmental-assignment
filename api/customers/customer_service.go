package customers

import "developmental-assignment/model"

func NewService(store CustomerStore) CustomerService {
	return &customerService{store: store}
}

type CustomerService interface {
	Create(customer *Customer) error
	GetByMessengerID(id string, opts model.GetOpts) (*Customer, error)
	ListAll(opts model.ListOpts) (*CustomerList, error)
}

type customerService struct {
	store CustomerStore
}

func (c *customerService) Create(customer *Customer) error {
	return c.store.Create(customer)
}

func (c *customerService) GetByMessengerID(id string, opts model.GetOpts) (*Customer, error) {
	return c.store.GetByMessengerID(id, opts)
}

func (c *customerService) ListAll(opts model.ListOpts) (*CustomerList, error) {
	return c.store.ListAll(opts)
}
