package customers

import (
	"developmental-assignment/model"
)

func NewList() *CustomerList {
	list := &CustomerList{}
	list.Init()
	return list
}

type CustomerList struct {
	model.BaseList
	items []*Customer
}

func (c *CustomerList) Model() model.Model {
	return &Customer{}
}

func (c *CustomerList) ItemsPtr() interface{} {
	return &c.items
}

func (c *CustomerList) Items() []*Customer {
	return c.items
}