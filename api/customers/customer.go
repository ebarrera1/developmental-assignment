package customers

import (
	"developmental-assignment/model"

)

type Customer struct {
	model.BaseModel
	Name      string `gorm:"type:varchar(255) null"`
	FBID      string `gorm:"type:varchar(100) null"`
	Birthdate string `gorm:"type:varchar(100)"`
}

func (c *Customer) GetName() string {
	return c.Name
}

func (c *Customer) SetFBName(arg string) *Customer {
	c.Name = arg
	return c
}

func (c *Customer) GetFBID() string {
	return c.FBID
}

func (c *Customer) SetFBID(arg string) *Customer {
	c.FBID = arg
	return c
}

func (c *Customer) GetBirthDate() string {
	return c.Birthdate
}

func (c *Customer) SetBirthDate(arg string) *Customer {
	c.Birthdate = arg
	return c
}
