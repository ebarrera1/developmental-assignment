package response

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"

	"developmental-assignment/model"
)

func JSON(any interface{}, w http.ResponseWriter) {
	json, err := json.Marshal(any)

	if err != nil {
		fmt.Println(err.Error())
		Error(err, w)
		return
	}

	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(json)
}

func Error(err error, w http.ResponseWriter) {
	data := map[string]interface{}{}
	switch err.(type) {
	case *model.Error:
		e := err.(*model.Error)
		data["code"] = e.Code
		w.WriteHeader(e.Status)
		break
	default:
		reg := regexp.MustCompile(`[^0-9]+`)
		code, _ := strconv.Atoi(reg.ReplaceAllString(err.Error(), ""))
		data["code"] = err.Error()
		w.WriteHeader(code)
	}

	j, err := json.Marshal(data)

	if err != nil {
		fmt.Println(err.Error())
	}

	if _, err := w.Write(j); err != nil {
		fmt.Println(err.Error())
	}
}
